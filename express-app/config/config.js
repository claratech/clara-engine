var os = require('os');
var networkInterfaces = os.networkInterfaces();
var endPointDetails = {
  hostName: os.hostname(),
  ipAddress: networkInterfaces['Ethernet'][1]['address'],
  portNumber: 8080
}

console.log(networkInterfaces);
module.exports = endPointDetails
