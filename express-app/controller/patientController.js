// contactController.js
const JSON = require('circular-json');
// Import contact model
const Patient = require('../models/patientModel');
secret = require('../config/jwt').jwt_secret
const jwt = require('jsonwebtoken')
const uuid=require('uuid/v3');
crypto = require('crypto');
// Handle index actions
exports.index = function (req, res) {
    Patient.get(function (err, patient) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Patient retrieved successfully",
            data: patient
        });
    });
};

// Handle create patient actions
exports.new = function (req, res) {
    var patient = new Patient();
    patient.name = req.body.name ? req.body.name : patient.name;
    patient.email = req.body.email;
    patient.phone = req.body.phone;
    let salt = secret;
    let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
    patient.password = hash

// save the contact and check for errors
patient.save(function (err) {
         if (err){
             res.json(err);
         }
         else{
        
res.json({
            message: 'New patient created!',
            data: {'ID':patient._id,
            'name':patient.name,
            'email':patient.email
        }       
        });
    }
});

};

// Handle view contact info
exports.view = function (req, res) {
    Patient.findById(req.params.patinet_id, function (err, patient) {
        if (err)
            res.send(err);
        res.json({
            message: 'patient details loading..',
            data: patient
        });
    });
};

exports.findByEmail = function (req, res) {
    Patient.findByEmail(req, function (err, patient) {
        if (err)
            res.send(err);
        res.json({
            message: 'patient details loading..',
            data: patient
        });
       
    });
    
};


// Handle update contact info
exports.update = function (req, res) {

Patient.findById(req.params.patient_id, function (err, patient) {
        if (err)
            res.send(err);

            patient.name = req.body.name ? req.body.name : patient.name;
            patient.gender = req.body.gender;
            patient.email = req.body.email;
            patient.phone = req.body.phone;
            patient.password = req.body.password;

// save the contact and check for errors
patient.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'patient Info updated',
                data: patient
            });
        });
    });
};

// Handle delete contact
exports.delete = function (req, res) {
    Patient.remove({
        _id: req.params.patient_id
    }, function (err, patient) {
        if (err)
            res.send(err);

res.json({
            status: "success",
            message: 'patient deleted'
        });
    });
};



// exports.hasAuthValidFields = (req, res, next) => {
//     let errors = [];
//     console.log(req.body.email);
//     if (req.body) {
//         if (!req.body.email) {
//             errors.push('Missing email field');
//         }
//         if (!req.body.password) {
//             errors.push('Missing password field');
//         }

//         if (errors.length) {
//             return res.status(400).send({errors: errors.join(',')});
//         } else {
//             return next();
//         }
//     } else {
//         return res.status(400).send({errors: 'Missing email and password fields'});
//     }
// };

// module.exports.isPasswordAndUserMatch = (req, res, next) => {
//     console.patch("321")
//     console.log("err"+err)
    
//     console.log("validation"+req.body.password)
//     Patient.findByEmail(req.body.email)
//     .then((user)=>{
//         if(!user[0]){
//             res.status(404).send({});
//         }else{
//             let passwordFields = user[0].password.split('$');
//             let salt = secret;
//              let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
//              console.log("hash"+hash)
//              console.log("passwordFields[1]"+passwordFields[0])
//             if (hash === passwordFields[0]) {
//                 req.body = {
//                     userId: user[0]._id,
//                     email: user[0].email,
//                     password: user[0].password,
//                     provider: 'email',
//                     // name: user[0].firstName + ' ' + user[0].lastName,
//                 };
//                 return next();
//             //    return res.json({
//             //         status: 'API Its Working',
//             //         message: 'Welcome to Clara Rest',
//             //      });
//             } else {
//                 return res.status(400).send({errors: ['Invalid e-mail or password']});
//             }
//         }
// });
// };

// exports.login = (req, res) => {
//     try {
//         // let refreshId = req.body.userId + jwtSecret;
//         // let salt = crypto.randomBytes(16).toString('base64');
//         // let hash = crypto.createHmac('sha512', salt).update(refreshId).digest("base64");
//         //req.body.refreshKey = salt;
//         let token = jwt.sign(req.body, secret);
//         // let b = new Buffer(hash);
//         // let refresh_token = b.toString('base64');
//         res.status(201).send({accessToken: token});
//     } catch (err) {
//         res.status(500).send({errors: err});
// }
// };
// exports.validJWTNeeded = (req, res, next) => {
//     if (req.headers['authorization']) {
//         try {
//             let authorization = req.headers['authorization'].split(' ');
//             if (authorization[0] !== 'Bearer') {
//                 return res.status(401).send();
//             } else {
//                 req.jwt = jwt.verify(authorization[1], secret);
//                 passwordAuth.then({})
                
//                 return next();
//             }

//         } catch (err) {
//             console.log("err"+err)
//             return res.status(403).send();
//         }
//     } else {
//         return res.status(401).send();
//     }

// };