// contactController.js
const JSON = require('circular-json');
// Import contact model
const User = require('../models/userModel');
secret = require('../config/jwt').jwt_secret
uuidFormat = require('../config/jwt').uiid
const uuid = require('uuid/v3');
const jwt = require('jsonwebtoken')
crypto = require('crypto')
var CryptoJS = require('crypto-js');
var QRCode = require('qrcode')
endPointDetails = require('../config/config');
// Handle index actions
exports.index = function (req, res) {
    console.log(req)
    User.get(function (err, User) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "User retrieved successfully",
            data: User
        });
    });
};

// Handle create User actions
exports.new = function (req, res) {
    console.log("A new request received at " + req.body.name);
    var user = new User();
    user.name = req.body.name ? req.body.name : User.name;
    user.email = req.body.email;
    user.ehrName = req.body.ehrName;
    user.role = req.body.role;
    user.phone = req.body.phone;
    user.uuid = uuid((req.body.email + req.body.name), uuidFormat)
    let salt = secret;
    let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
    user.password = hash

    // save the contact and check for errors
    user.save(function (err) {
        if (err) {
            res.json(err);
        }
        else {
            // var segs = [
            //     { data: 'UIID :'+ user.uuid+'\n', mode: 'Byte' },
            //     { data: 'IP :'+'192.168.200.101 \n', mode: 'Byte' },
            //     { data: 'Host Name :'+ 'www.test.com \n', mode: 'Byte' },
            //     { data: 'Token: '+ 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YzUwYTg3MjgwYzlkNzEzMzA3MzFjYmUiLCJlbWFpbCI6InRlc3QxMTJAdGVzdC5jb20iLCJwYXNzd29yZCI6InRITnZGZFEzMmFTZlVJd2ZPWlE4OXREOW05b3NWUlo5cittSlNEaHdheVd2bGNML3VXUHpodWE5NXBTWktqZk1TSlBnVjN1bmJ2aGhOdElhSGNUbElnPT0iLCJwcm92aWRlciI6ImVtYWlsIiwiaWF0IjoxNTQ4Nzg5OTgzfQ.Teudv6r0s_I9FpKbDDfb8DFIoD7J0ZQ6c2nRR8M71bk', mode: 'Byte' }
            //   ]
            body = {
                userId: user._id,
                email: user.email,
                password: hash,
                uuid: user.uuid,
                provider: 'email',
                // name: user[0].firstName + ' ' + user[0].lastName,
            };
            let token = jwt.sign(body, secret);
            var testdata;
            var outgoingQrData = {
                uuid: user.uuid,
                name: user.name,
                role: user.role,
                port: endPointDetails.portNumber,
                hostName: endPointDetails.hostName,
                ipAddress: endPointDetails.ipAddress,
                token: token
            }
            console.log('endPointDetails.hostName: ' + endPointDetails.hostName)
            console.log('Body: ' + outgoingQrData.token)
            var encryptedData = CryptoJS.AES.encrypt(JSON.stringify(outgoingQrData), 'secret key 123').toString();
            QRCode.toDataURL(encryptedData)
                .then(url => {
                    console.log(url)

                    // let

                    testdata = url
                    res.render('qrCode', {
                        userData: {
                            'ID': user._id,
                            'name': user.name,
                            'email': user.email,
                            'role': user.role,
                            'ehrName': user.ehrName,
                            'phone': user.phone,
                            'uuid': user.uuid,
                            'qrCode': testdata

                        }
                    });
                })
                .catch(err => {
                    console.error(err)
                })
            //     res.render('qrCode', { userData: {'ID':user._id,
            //     'name':user.name,
            //     'email':user.email,
            //     'qrCode':testdata

            //     } 
            // });

        }
    });

};

// Handle view contact info
exports.view = function (req, res) {
    User.findById(req.params.patinet_id, function (err, User) {
        if (err)
            res.send(err);
        res.json({
            message: 'User details loading..',
            data: User
        });
    });
};

exports.findByEmail = function (req, res) {
    User.findByEmail(req, function (err, User) {
        if (err)
            res.send(err);
        res.json({
            message: 'User details loading..',
            data: User
        });

    });

};

exports.findByUuid = function (req, res) {
    console.log('updateDevice here')

    var decryptedDatatest = CryptoJS.AES.decrypt(req.body.data.toString(), 'secret key 123');
    var plaintext = decryptedDatatest.toString(CryptoJS.enc.Utf8);
    console.log('test ' + plaintext);
    // plaintextObj=JSON.parse(plaintext.data);
    // var decryptedData = CryptoJS.AES.decrypt(JSON.stringify(outgoingQrData), 'secret key 123').toString();
    plaintext = JSON.parse(plaintext)
    //var user = new User();
    var uuid = plaintext.uuid;
    console.log(uuid);
    User.findByUuidContainsPassword(uuid)
        .then((user) => {
            // if(!user[0]){
            if (!user) {

                res.status(404).send({});
            }
            var respose = {
                uuid: user.uuid,
                uniqueId: user.deviceMac,
                status: user.verified
            }
            var encryptedData = CryptoJS.AES.encrypt(JSON.stringify(respose), 'secret key 123').toString();

            res.status(200).send(encryptedData);
        });

};

// Handle update contact info
exports.update = function (req, res) {
    console.log('here')
    //var user = new User();
    var uuid = req.body.uuid;
    var hash;
    console.log(uuid);
    User.findByUuidContainsPasswordAdmin(uuid)
        .then((user) => {
            // if(!user[0]){
            if (!user) {

                res.status(404).send({});
            }
            console.log(user.deviceMac);
            user.deviceMac = req.body.deviceMac
            user.verified = req.body.verified
            user.password = hash
            // save the contact and check for errors
            body = {
                userId: user._id,
                email: user.email,
                password: hash,
                uuid: user.uuid,
                provider: 'email',
                // name: user[0].firstName + ' ' + user[0].lastName,
            };
            let token = jwt.sign(body, secret);
            User.findOneAndUpdate({ "uuid": uuid }, { "$set": { "verified": user.verified } }).exec(function (err, userDB) {
                if (err) {
                    res.json(err);
                }
                else {
                    var testdata;
                    var outgoingQrData = {
                        uuid: user.uuid,
                        name: user.name,
                        role: user.role,
                        port: endPointDetails.portNumber,
                        hostName: endPointDetails.hostName,
                        ipAddress: endPointDetails.ipAddress,
                        token: token

                    }
                    var encryptedData = CryptoJS.AES.encrypt(JSON.stringify(outgoingQrData), 'secret key 123').toString();
                    QRCode.toDataURL(encryptedData)
                        .then(url => {
                            //console.log(url)
                            testdata = url
                            res.render('qrCode', {
                                userData: {
                                    'ID': User._id,
                                    'name': user.name,
                                    'email': user.email,
                                    'role': user.role,
                                    'ehrName': user.ehrName,
                                    'phone': user.phone,
                                    'deviceMac': user.deviceMac,
                                    'verified': user.verified,
                                    'make':user.make,
                                    'model':user.model,
                                    'os':user.osVersion,
                                    'ip':user.ipAddress,
                                    'isPinOrFingerprintSet':user.isPinOrFingerprintSet,
                                    'isTablet':user.isTablet,
                                    'deviceType':user.deviceType,
                                    'deviceFriendlyName':user.deviceFriendlyName,
                                    'api':user.api,
                                    'uuid': user.uuid,
                                    'qrCode': testdata

                                }
                            });
                        })
                        .catch(err => {
                            console.error(err)
                        })
                }
            });
        });
};

//handle update mac
// Handle update contact info
exports.updateDevice = function (req, res) {
    console.log('updateDevice here')
    // var data=  {
    //     brand: 'apple',
    //     deviceId: 'iPad7,6',
    //     deviceName: 'Ameer’s iPad',
    //     ipAddress: '192.168.1.102',
    //     uniqueId: '6666-1111-1111-3333',
    //     osVersion: '12.1',
    //     isPinOrFingerprintSet: 'true',
    //     isTablet: 'true',
    //     deviceType: 'tablet'
    //     }
    // var encryptedDatatest = CryptoJS.AES.encrypt(JSON.stringify(data), 'secret key 123').toString();    
    // var decryptedDatatest = CryptoJS.AES.decrypt(encryptedDatatest.toString(), 'secret key 123');
    // var plaintext = decryptedDatatest.toString(CryptoJS.enc.Utf8);
    // console.log('test '+encryptedDatatest);
    var decryptedDatatest = CryptoJS.AES.decrypt(req.body.data.toString(), 'secret key 123');
    var plaintext = decryptedDatatest.toString(CryptoJS.enc.Utf8);
    console.log('test ' + plaintext);
    // plaintextObj=JSON.parse(plaintext.data);
    // var decryptedData = CryptoJS.AES.decrypt(JSON.stringify(outgoingQrData), 'secret key 123').toString();
    plaintext = JSON.parse(plaintext)
    //var user = new User();
    var uuid = plaintext.uuid;
    console.log(uuid);
    User.findByUuidContainsPassword(uuid)
        .then((user) => {
            // if(!user[0]){
            if (!user) {

                res.status(404).send({});
            }
            // console.log(user.deviceMac);
            // user.deviceMac=plaintext.uniqueId
            //user.verified=req.body.verified
            // save the contact and check for errors
            var updates={
                deviceMac:plaintext.deviceId, 
                verified: 'pending',
                make:plaintext.brand,
                model:plaintext.model,
                os:plaintext.sysVersion,
                ip:plaintext.ip,
                isPinOrFingerprintSet:plaintext.isPinSet,
                isTablet:plaintext.isTablet,
                deviceType:plaintext.type,
                api:plaintext.api,
                deviceFriendlyName:plaintext.deviceFriendlyName,
            }
            User.findOneAndUpdate({ "uuid": plaintext.uuid }, { "$set": updates }).then((updateUser) => {
                var respose = {
                    uuid: updateUser.uuid,
                    uniqueId: updateUser.deviceMac,
                    status: 'pending'
                }
                var encryptedData = CryptoJS.AES.encrypt(JSON.stringify(respose), 'secret key 123').toString();

                res.status(200).send(encryptedData);
            });
            // User.findOneAndUpdate({ "uuid": plaintext.uniqueId }, { "$set": { "deviceMac": plaintext.deviceId,"verified":"pending"}}).exec(function(err, updateUser){
            //     if(err) {
            //         console.log(err);
            //         res.status(500).send(err);
            //     } else
            //      { 
            //         //console.log(updateUser.deviceMac)
            //     var respose=    {
            //             uuid:updateUser.uuid ,
            //             uniqueId:updateUser.deviceMac,
            //          status:'pending'
            //      }
            //      var encryptedData = CryptoJS.AES.encrypt(JSON.stringify(respose), 'secret key 123').toString();

            //              res.status(200).send(encryptedData);
            //     }
            //  });
            // user.update(function (err) {
            //             if (err){
            //                 res.json(err);
            //             }
            //             else{
            //                 console.log(user.deviceMac)
            //                 user.deviceMac=plaintext.uniqueId

            //                  //var encryptedData = CryptoJS.AES.encrypt(JSON.stringify(outgoingQrData), 'secret key 123').toString();
            //                  res.json({
            //                     message: 'User details loading..',
            //                     data: 'test'
            //                 });
            //         }
            //         });
        });
};

// Handle delete contact
exports.delete = function (req, res) {
    User.remove({
        _id: req.params.User_id
    }, function (err, User) {
        if (err)
            res.send(err);

        res.json({
            status: "success",
            message: 'User deleted'
        });
    });
};



// exports.hasAuthValidFields = (req, res, next) => {
//     let errors = [];
//     console.log(req.body.email);
//     if (req.body) {
//         if (!req.body.email) {
//             errors.push('Missing email field');
//         }
//         if (!req.body.password) {
//             errors.push('Missing password field');
//         }

//         if (errors.length) {
//             return res.status(400).send({errors: errors.join(',')});
//         } else {
//             return next();
//         }
//     } else {
//         return res.status(400).send({errors: 'Missing email and password fields'});
//     }
// };

// module.exports.isPasswordAndUserMatch = (req, res, next) => {
//     console.patch("321")
//     console.log("err"+err)

//     console.log("validation"+req.body.password)
//     Patient.findByEmail(req.body.email)
//     .then((user)=>{
//         if(!user[0]){
//             res.status(404).send({});
//         }else{
//             let passwordFields = user[0].password.split('$');
//             let salt = secret;
//              let hash = crypto.createHmac('sha512', salt).update(req.body.password).digest("base64");
//              console.log("hash"+hash)
//              console.log("passwordFields[1]"+passwordFields[0])
//             if (hash === passwordFields[0]) {
//                 req.body = {
//                     userId: user[0]._id,
//                     email: user[0].email,
//                     password: user[0].password,
//                     provider: 'email',
//                     // name: user[0].firstName + ' ' + user[0].lastName,
//                 };
//                 return next();
//             //    return res.json({
//             //         status: 'API Its Working',
//             //         message: 'Welcome to Clara Rest',
//             //      });
//             } else {
//                 return res.status(400).send({errors: ['Invalid e-mail or password']});
//             }
//         }
// });
// };

// exports.login = (req, res) => {
//     try {
//         // let refreshId = req.body.userId + jwtSecret;
//         // let salt = crypto.randomBytes(16).toString('base64');
//         // let hash = crypto.createHmac('sha512', salt).update(refreshId).digest("base64");
//         //req.body.refreshKey = salt;
//         let token = jwt.sign(req.body, secret);
//         // let b = new Buffer(hash);
//         // let refresh_token = b.toString('base64');
//         res.status(201).send({accessToken: token});
//     } catch (err) {
//         res.status(500).send({errors: err});
// }
// };
// exports.validJWTNeeded = (req, res, next) => {
//     if (req.headers['authorization']) {
//         try {
//             let authorization = req.headers['authorization'].split(' ');
//             if (authorization[0] !== 'Bearer') {
//                 return res.status(401).send();
//             } else {
//                 req.jwt = jwt.verify(authorization[1], secret);
//                 passwordAuth.then({})

//                 return next();
//             }

//         } catch (err) {
//             console.log("err"+err)
//             return res.status(403).send();
//         }
//     } else {
//         return res.status(401).send();
//     }

// };

exports.indexHomeData = function (req, res) {
    console.log(req)
    User.get(function (err, User) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        // res.send(User)
        res.render('home', { User: User })
        // return next(User);
        // res.json({
        //     status: "success",
        //     message: "User retrieved successfully",
        //     data: User
        // });
    });
};

exports.updateGrid = function (req, res) {
    console.log('updateGrid')
    //var user = new User();
    //var uuid=req.body.uuid;
    var uuid = req.params.uuid;
    console.log(uuid);
    User.findByUuidContainsPasswordAdmin(uuid)
        .then((user) => {
            // if(!user[0]){
            if (!user) {

                res.status(404).send({});
            }
            else {
                body = {
                    userId: user._id,
                    email: user.email,
                    password: user.password,
                    uuid: user.uuid,
                    provider: 'email',
                    // name: user[0].firstName + ' ' + user[0].lastName,
                };

                let token = jwt.sign(body, secret);
                var outgoingQrData = {
                    uuid: user.uuid,
                    name: user.name,
                    role: user.role,
                    port: endPointDetails.portNumber,
                    hostName: endPointDetails.hostName,
                    ipAddress: endPointDetails.ipAddress,
                    token: token
                }
                var encryptedData = CryptoJS.AES.encrypt(JSON.stringify(outgoingQrData), 'secret key 123').toString();
                QRCode.toDataURL(encryptedData)
                    .then(url => {
                        //  console.log(url)

                        // letY

                        testdata = url
                        res.render('qrCode', {
                            userData: {
                                'ID': user._id,
                                'name': user.name,
                                'email': user.email,
                                'role': user.role,
                                'ehrName': user.ehrName,
                                'phone': user.phone,
                                'deviceMac': user.deviceMac,
                                'verified': user.verified,
                                'make':user.make,
                                'model':user.model,
                                'os':user.osVersion,
                                'ip':user.ipAddress,
                                'isPinOrFingerprintSet':user.isPinOrFingerprintSet,
                                'isTablet':user.isTablet,
                                'deviceType':user.deviceType,
                                'deviceFriendlyName':user.deviceFriendlyName,
                                'api':user.api,
                                'uuid': user.uuid,
                                'qrCode': testdata

                            }
                        });
                        return
                    })
                    .catch(err => {
                        console.error(err)
                    })
                return
            }
        })
        .catch(err => {
            console.error(err)
        })

};