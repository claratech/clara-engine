// patientModel.js

var mongoose = require('mongoose');

// Setup schema
var patientSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    password:{
        type: String,
        select:false
    },
});


// Export Patient model
var Patient = module.exports = mongoose.model('patient', patientSchema);

module.exports.get = function (callback, limit) {
    Patient.find(callback).limit(limit);
}

module.exports.findByEmail = function(email) {
    return Patient.findOne({email: email});
};
module.exports.findByEmailContainsPassword = function(email) {
    return Patient.findOne({email: email}).select("+password");


};
// module.exports.findByEmailJson=function(email){
//     lean().exec(function (err, patients) {
//     return res.end(JSON.stringify(patients.findOne({email: email})));
// });
// }
//  module.exports.findByEmail = (email) => {
//      return Patient.find({email: email});
//  };

