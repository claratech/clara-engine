// doctorModel.js

var mongoose = require('mongoose');

// Setup schema
var userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    ehrName: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    password :{
        type: String,
        select:false
    },
    uuid: String,
    deviceMac: {
        type: String,
        default:'not Set'
    },
    verified: {
        type: String,
        default:'null'
    },
    make:{
        type:String
    },
    model:{
        type:String
    },
    os:{
        type:String
    },
    ip:{
        type:String
    },
    isPinOrFingerprintSet:{
        type:String
    },
    isTablet:{
        type:String
    },
    deviceType:{
        type:String
    },
    deviceFriendlyName:{
        type:String
    },
    api:{
        type:String
    }
});


// Export User model
var User = module.exports = mongoose.model('user', userSchema);

module.exports.get = function (callback, limit) {
    User.find(callback).limit(limit);
}

module.exports.findByEmail = function(email) {
    return User.findOne({email: email});
};
module.exports.findByEmailContainsPassword = function(email) {
    return User.findOne({email: email}).select("+password");
};
module.exports.findByUuid = function(uuid) {
    return User.findOne({uuid: uuid});
};
module.exports.findByUuidContainsPassword = function(uuid) {
    return User.findOne({uuid: uuid, verified: { $nin: ["rejected"] }}).select("+password");
};
module.exports.findByUuidContainsPasswordAdmin = function(uuid) {
    return User.findOne({uuid: uuid}).select("+password");
};
// module.exports.findByEmailJson=function(email){
//     lean().exec(function (err, Users) {
//     return res.end(JSON.stringify(Users.findOne({email: email})));
// });
// }
//  module.exports.findByEmail = (email) => {
//      return User.find({email: email});
//  };

