const Joi = require('joi');
const CircularJSON = require('circular-json');

const schema = Joi.object().keys({
    name: Joi.string().required().error(()=>'Name is Required'),
    phone: Joi.number().error(()=>'Number Only'),
    email: Joi.string().email({ minDomainAtoms: 2 }),
    password: Joi.string()
});
 
exports.validate = function (req, res,next) {
    const result = Joi.validate(req.body, schema,{abortEarly: false});
        if (result.error!=null){
        console.log("error"+result.error);
        res.status(400)
        .json({
            message: 'patient details loading..',
            data: ""+result.error
        });
      
        }else{
        // res.json({
        //     message: 'patient details loading..',
        //     data: result
        // });
        next();
    }
};
// // Return result.
// const result = Joi.validate({ username: 'abc', birthyear: 1994 }, schema);
// // result.error === null -> valid
 
// // You can also pass a callback which will be called synchronously with the validation result.
// Joi.validate({ username: 'abc', birthyear: 1994 }, schema, function (err, value) { });  // err === null -> valid
 