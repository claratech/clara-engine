/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */

'use strict'
const { Builder, By, Key, until } = require('selenium-webdriver');
var Login = function (driver) {
    // const email = element(by.id(‘placeholder_email’))
    // var login = element(by.id(‘button_signin’))  
    // var forgotpass = element(by.id(‘link_forgotpassword’))  

    this.withEmail = async function (data, driver) {
        try {


            await driver.findElement(By.name('USERNAME')).sendKeys(data.username);
            await driver.findElement(By.name('PASSWORD')).sendKeys(data.pass, Key.RETURN);
            // await driver.wait(until.elementIsVisible(By.id('loginbutton')), 10000);
            // await driver.findElement(By.id('loginbutton')).sendKeys(Key.RETURN);
        } catch (error) {
            console.log(error)
        }
        // await driver.findElement(By.name('PASSWORD')).sendKeys(data.pass, Key.RETURN);
        // await driver.findElement(By.name('loginbutton')).sendKeys('webdriver', Key.RETURN);
        //element(by.id('USERNAME')).typeText(data.email+'\n'+data.pass+'\n');
        // email.typeText(data.email+‘\n’+data.password+‘\n’);
    };
};

module.exports = new Login;
