// Filename: api-routes.js

const validations = require('../models/validation/bodyValidation');
const validate = require('express-validation');

const google = require('../selenium/openGoogle');
const path = require('path');
// Initialize express router
let router = require('express').Router();
const validation = require("../auth/validation");
var swaggerJSDoc = require('swagger-jsdoc');
// Import patients controller
var patientController = require('../controller/patientController');
var userController = require('../controller/userController');
const Joi = require('joi');
// /
//  * @swagger
//  * definition:
//  *   users:
//  *     properties:
//  *       name:
//  *         type: string
//  *       email:
//  *         type: string
//  *       phone:
//  *         type: integer
//  *       password:
//  *         type: string
//  */

// /**
//  * @swagger
//  * /api/:
//  *   get:
//  *     tags:
//  *       - users
//  *     description: Returns all users
//  *     produces:
//  *       - application/json
//  *     responses:
//  *       200:
//  *         description: An array of users
//  *         schema:
//  *           $ref: '#/definitions/users'
//  */
// router.get('/', function (req, res) {
//     res.json({
//        status: 'API Its Working',
//        message: 'Welcome to Clara Rest',
//     });
// });


// patients routes
/**
 * @swagger
 * /api/patients:
 *   get:
 *     tags:
 *       - users
 *     security:
 *       - Bearer: []
 *     description: Returns all users
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of users
 *         schema:
 *           $ref: '#/definitions/users'
 */
/**
* @swagger
* /api/patients:
*   post:
*     tags:
*       - users
*     description: Returns all 
*     security:
*       - Bearer: []
*     produces:
*       - application/json
*     consumes:
*       - application/json
*     parameters: [{name: "sentences",in: "body",description: "Newline delimited sentences",required: true,schema: {type: "string"},}]
*     responses:
*       200:
*         description: An array of users
*         schema:
*           $ref: '#/definitions/users'
*/
router.route('/patients')
  .get(validation.validJWTNeeded, patientController.index)
  .post(validations.validate, patientController.new);

// router.route('/auth')
// .post(
//      patientController.hasAuthValidFields

//     // patientController.isPasswordAndUserMatch,
//     // patientController.login
//   );
router.route('/auth').post(validation.hasAuthValidFields,
  validation.isPasswordAndUserMatchForToken, validation.login);
/**
* @swagger
* /api/test:
*   get:
*     tags:
*       - users
*     description: Returns all users
*     produces:
*       - application/json
*     responses:
*       200:
*         description: An array of users
*         schema:
*           $ref: '#/definitions/users'
*/

// router.get('/', function (req, res) {
//     res.json({
//        status: 'API Its Working',
//        message: 'Welcome to Clara Rest',
//     });
// });
router.route('/test')
  .get(async function (req, res) {
    // google.google().then(()=>{
    //   res.json({
    //     status: 'API Its Working',
    //     message: 'Welcome to Clara Rest',
    //   })}
    // ).catch(() => { console.log("error") })
    await google.google();
      res.json({
        status: 'API Its Working',
        message: 'Welcome to Clara Rest',})

  });


// patients routes
/**
 * @swagger
 * /api/patients/:patients_id:
 *   get:
 *     tags:
 *       - users
 *     description: Returns all users
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of users
 *         schema:
 *           $ref: '#/definitions/users'
 */

router.route('/patients/:patients_id')
  .get(patientController.view)
  .patch(patientController.update)
  .put(patientController.update)
  .delete(patientController.delete);
// Export API routes

// patients routes
/**
 * @swagger
 * /api/patients:
 *   get:
 *     tags:
 *       - users
 *     security:
 *       - Bearer: []
 *     description: Returns all users
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of users
 *         schema:
 *           $ref: '#/definitions/users'
 */
/**
* @swagger
* /api/patients:
*   post:
*     tags:
*       - users
*     description: Returns all 
*     security:
*       - Bearer: []
*     produces:
*       - application/json
*     consumes:
*       - application/json
*     parameters: [{name: "sentences",in: "body",description: "Newline delimited sentences",required: true,schema: {type: "string"},}]
*     responses:
*       200:
*         description: An array of users
*         schema:
*           $ref: '#/definitions/users'
*/
router.route('/user')
  .get(userController.index)
  .post(userController.new);

router.route('/updateUser')
  .post(userController.update);
//.get(userController.updateGird)

/**
* @swagger
* /api/device-registration:
*   post:
*     tags:
*       - users
*     description: Returns all 
*     security:
*       - Bearer: []
*     produces:
*       - application/json
*     consumes:
*       - application/json
*     parameters: [{name: "Uniquie ID",in: "body",description: "Unique ID for device registration",required: true,schema: {type: "string"},}]
*     responses:
*       200:
*         description: An array of users
*         schema:
*           $ref: '#/definitions/users'
*/
router.route('/device-registration')
  .post(validation.validJWTNeeded, userController.updateDevice)


/**
* @swagger
* /api/device-registration-status:
*   post:
*     tags:
*       - users
*     description: Returns all 
*     security:
*       - Bearer: []
*     produces:
*       - application/json
*     consumes:
*       - application/json
*     parameters: [{name: "Uniquie ID",in: "body",description: "Unique ID for getting device-registration-status",required: true,schema: {type: "string"},}]
*     responses:
*       200:
*         description: An array of users
*         schema:
*           $ref: '#/definitions/users'
*/
router.route('/device-registration-status')
  .post(validation.validJWTNeeded, userController.findByUuid)



module.exports = router;




