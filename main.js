const electron = require("electron"),
  app = electron.app,
  BrowserWindow = electron.BrowserWindow;
const { Menu, Tray } = require('electron')

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow();
  mainWindow.maximize()
  mainWindow.loadURL(`file://${__dirname}/index.html`);
  //mainWindow.webContents.openDevTools();
  mainWindow.on("close", () => {
    mainWindow.webContents.send("stop-server");
  });
  mainWindow.on("closed", () => {
    mainWindow = null;
  });
}

app.on("ready", () => {
  tray = new Tray('clara2-st.jpg')
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Empty Application',
      enabled: false
    },

    {
      label: 'Settings',
      click: function () {
        createWindow();
        console.log("Clicked on Help")


        //create a server object:

      }
    },

    {
      label: 'Help',
      click: 
        console.log("Clicked on Help")
    }
  ])
  tray.setToolTip('This is my application.')
  tray.setContextMenu(contextMenu)
});
app.on("browser-window-created", function (e, window) {
  window.setMenu(null);
});

app.on("window-all-closed", function () {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

// app.on("activate", function() {
//   if (mainWindow === null) {
//     createWindow();
//   }
// });
